from taschenrechner.services.math import add
import pytest

# Tests should be prefixed with test_
def test_add_positive_integers():
    result = add(5, 10)

    assert 15 == result


def test_add_negative_integers():
    result = add(-5, -10)

    assert -15 == result


def test_add_validates_inputs():
    with pytest.raises(ValueError):
        add("2", "1")