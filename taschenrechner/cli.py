import click
from taschenrechner.services import math


@click.command()
@click.option('--x', type=int)
@click.option('--y', type=int)
def add(x, y):
    click.echo(math.add(x, y))