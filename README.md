### First Time Setup
1. Install [brew](https://brew.sh/)
2. `brew install python3`
3. Install [poetry](https://python-poetry.org/docs/#osx-linux-bashonwindows-install-instructions)
4. Run `poetry install`
5. Test everything is working with `poetry run add --x 1 --y 2`
6. Run tests using `poetry run pytest`


### Adding Features
1. Implement the function/business logic in `services/math.py`
2. Write tests for the above function in `test_taschenrechney.py`.  (Optional Hard Mode: Write the tests first and then write code to test them.)
3. Implement a CLI interface in `cli.py` so you can easily test the feature.